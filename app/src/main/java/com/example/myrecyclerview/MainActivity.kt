package com.example.myrecyclerview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val attractionsList: List<Attractions> by lazy {
        arrayListOf(
            Attractions(
                "เกาะสมุย",
                "อำเภอเกาะสมุย สุราษฎร์ธานี",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTX22c-UvSzxFwk-0BlsEIHEKgeRSBFHmyxKFZZs6ZRhzNvqoMU"
            ),
            Attractions(
                "น้ำตกห้วยแม่ขมิ้น",
                "ตำบล แม่กระบุง อำเภอศรีสวัสดิ์ กาญจนบุรี",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQxwx9zsPP7uY9-GCDVeuV0TCK0S_OIHxGhl9v5b0NljMg4vu3d"
            ),
            Attractions(
                "ภูกระดึง",
                "อำเภอภูกระดึง จังหวัดเลย",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTBQ0JtPrQwiCEY8_aX3VJPc9LiDLoekkjq9Iwov9WvCBNHBIhA"
            ),
            Attractions(
                "เขาค้อ",
                "อำเภอเขาค้อ จังหวัดเพชรบูรณ์",
                "https://i0.wp.com/travelblog.expedia.co.th/wp-content/uploads/2017/12/cover-%E0%B9%80%E0%B8%82%E0%B8%B2%E0%B8%84%E0%B9%89%E0%B8%AD.jpg?resize=1140%2C550&ssl=1"
            ),
            Attractions(
                "อุทยานแห่งชาติภูสอยดาว",
                "ตำบล ห้วยมุ่น อำเภอน้ำปาด อุตรดิตถ์",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcREP_PeFZ8Qh7XTCUaZxGv-DEEuxhrS6NsUodm3cpPddfodCR_A"
            )
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {

        attractions_recycler_view.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = AttractionsAdapter(
                Glide.with(applicationContext),
                attractionsList
            )
        }

    }
}
