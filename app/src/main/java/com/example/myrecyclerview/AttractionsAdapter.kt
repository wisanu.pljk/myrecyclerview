package com.example.myrecyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_attractions.view.*
import java.net.URI

class AttractionsAdapter(
    private var glide: RequestManager,
    private val attractionsList: List<Attractions>
) : RecyclerView.Adapter<AttractionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_attractions, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = attractionsList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(glide, attractionsList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(glide: RequestManager, attractions: Attractions) {
            itemView.apply {
                attractions_title.text = attractions.title
                attractions_location.text = attractions.location

                glide.load(attractions.imageURL!!)
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .into(attractions_image)
            }
        }
    }
}