package com.example.myrecyclerview

data class Attractions(
    var title: String?,
    var location: String?,
    var imageURL: String?
)